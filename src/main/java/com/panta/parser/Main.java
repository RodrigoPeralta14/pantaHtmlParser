package com.panta.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static final String COMANDO_SALIR = "exit";
    public static final String COMANDO_CONTINUAR = "continue";
    public static final String COMANDO_AYUDA = "help";

    public static final String MENSAJE_INICIAL = "Ingresa un archivo HTML ('continue' para continuar): ";
    public static final String MENSAJE_EXITO = " agregado a la lista!";
    public static final String MENSAJE_FINAL = "Resultado final: ";
    public static final String MENSAJE_ERROR_NO_HTML = "No es un Archivo .html valido, ";
    public static final String MENSAJE_ERROR_LECTURA_ARCHIVO = "Error leyendo el archivo!";
    public static final String MENSAJE_ERROR_GENERICO = "Error de sistema.";
    public static final String MENSAJE_ERROR_ARCHIVO_NO_ENCONTRADO = "No se encuentra el archivo especificado: ";
    public static final String MENSAJE_GREETING = "Panta HTML Parser 1.0";
    public static final String MENSAJE_DE_AYUDA = "Bienvenido! Escriba a continuacion la ruta a los archivos que desee parsear. " +
            "\nEscriba '"+COMANDO_SALIR+"' para cerrar el programa o '"+COMANDO_CONTINUAR+"' para parsear los archivos ingresados \n";


    public static void main(String[] args) {

        System.out.print(Main.MENSAJE_GREETING + "\n");
        System.out.print(Main.MENSAJE_DE_AYUDA);
        Main.inicializar();

    }

    public static void inicializar() {

        List<String> listaDeHtmlsAParsear = new ArrayList<>();
        Scanner reader = new Scanner(System.in);

        System.out.print(Main.MENSAJE_INICIAL);

        String input = reader.next();
        while(!input.equals(COMANDO_SALIR) && !input.equals(COMANDO_CONTINUAR) && !input.equals(COMANDO_AYUDA)) {

            if(input.matches("^.*.html$")) {
                listaDeHtmlsAParsear.add(input);
                System.out.print(input + Main.MENSAJE_EXITO + "(" + listaDeHtmlsAParsear.toString() + ")" + "\n" );
                System.out.print(Main.MENSAJE_INICIAL);
            } else {
                System.out.print(Main.MENSAJE_ERROR_NO_HTML + Main.MENSAJE_INICIAL);
            }

            input = reader.next();
        }

        if (input.equals(COMANDO_SALIR)) {

            System.exit(0);
            return;
        }  else if (input.equals(COMANDO_AYUDA)) {

            System.out.print(Main.MENSAJE_DE_AYUDA);
            Main.inicializar();
            return;
        } else if(input.equals(COMANDO_CONTINUAR)) {

            Main.parsearHtmls(listaDeHtmlsAParsear);

        }



    }

    public static void parsearHtmls(List<String> rutas) {

        String finalPaths = "";

        try {

            for(String ruta : rutas) {

                String fileName = ruta;

                File input = new File(ruta);

                try {

                    Document document = Jsoup.parse(input, "UTF-8");

                    Elements scriptTags = document.select("script");
                    for (Element scriptTag : scriptTags) {

                        String src = scriptTag.attr("src");
                        if(src.matches("^js\\/internos\\/.*[^\"].js$")) {

                            String srcFinal = src.replace( "js/internos/", "" );
                            if(!finalPaths.contains(srcFinal)) {

                                finalPaths = finalPaths + " " + srcFinal;

                            }

                        }


                    }

                    System.out.print("Parseado con exito el archivo: " + ruta + "(" + scriptTags.size() + " elementos encontrados) \n");

                } catch(FileNotFoundException exception) {

                    System.out.print(Main.MENSAJE_ERROR_ARCHIVO_NO_ENCONTRADO + ruta + "\n");

                } catch(IOException exception) {

                    System.out.print(Main.MENSAJE_ERROR_LECTURA_ARCHIVO + ruta + "\n");

                } catch(Exception exception) {

                    System.out.print(Main.MENSAJE_ERROR_GENERICO + "\n");

                }

            }

            System.out.print(Main.MENSAJE_FINAL + finalPaths + "\n");

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            Main.inicializar();

        }


    }
}
